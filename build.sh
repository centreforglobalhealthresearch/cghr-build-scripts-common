#!/bin/sh

echo  " 1.hc\n 2.hcamp\n 3.isha\n 4.mvm\n 5.logistics \n  6.cod-anand \n 7.mvm_punjab \n 8.cod-punjab" 
read -p "Project Name= " project
echo   "Building project ..."$project


cd $project
rm -rf release/*
echo   "Cleaning release ..."
cd services
rm -r build/libs/*.war
echo "Building services..."
gradle build
cp build/libs/*.war ../release/
cd ../ui

echo "Building ui..."
grunt
cp -r bin/* ../release/
cd ..
cd release/
unzip services.war
rm services.war
echo "Packaging ui and services together ..."
zip  -r $project.war .
echo "Cleaning up junk ..."
find . \! -name *.war -delete
mkdir $project
unzip $project.war -d $project/
echo "Task Completed..."
exit;
