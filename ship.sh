#!/bin/sh

echo  " 1.hc\n 2.hcamp\n 3.isha\n 4.mvm\n 5.logistics \n 6.cod-anand \n 7.mvm_punjab \n 8.cod-punjab" 
read -p "Project Name= " project
echo   "Building project ..."$project


cd $project
rm -rf package/*
echo   "Cleaning Package ..."
cd services
rm -r build/libs/*.war
echo "Building services..."
gradle build
cd ../package
cp ../services/build/libs/services.war .
mkdir isha-logistics
mkdir isha-logistics/services
unzip services.war -d isha-logistics/services/
rm services.war
cp -r ../assets/* isha-logistics
cp -r ../services/lib isha-logistics
cp -r ../ui/dist/* isha-logistics/services/
rm *.zip
zip -r isha-logistics.zip  ./*

