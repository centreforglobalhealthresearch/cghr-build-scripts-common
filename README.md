# Common Build Scripts for CGHR Projects  

Build Scripts to run in Development Environment to Build the application in Development/Debugging mode or in package application  

### Project Structure  

-ProjectName  
|----Project  
        |--release  
		|--package (Packaged application after running package.sh)  
        |--ui (Git Repo)  
        |--services (Git Repo)  		
--build.sh  
--launch.sh  
--package.sh  
--start.sh (Only for Blood Logistics application)  
--ui.sh (Used in Development Mode for Debugging in IDE)  
--servive.sh (Used in Development Mode for Debugging in IDE)  
--updateUI.sh  
--offlineAppDeploy.sh  
--jetty-runner.jar  